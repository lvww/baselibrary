package com.android.base

interface IBaseView {

    /**
     * 执行的第一个方法可再次初始化一些参数.
     */
    fun initParam() {}

    /**
     * LayoutId.
     */
    fun initLayoutId(): Int

    /**
     * ViewModel id.
     */
    fun initVariableId(): Int

    /**
     * Init view.
     */
    fun initView()

    /**
     * 注册监听之类的.
     */
    fun initObserver() {}

    /**
     * 初始化一些必要参数OnStart之前的最后一个方法
     */
    fun initData() {}
}