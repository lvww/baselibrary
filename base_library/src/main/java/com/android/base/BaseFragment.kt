package com.android.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.reflect.ParameterizedType
import java.util.Objects

abstract class BaseFragment<V : ViewDataBinding, VM : BaseViewModel<*, *>> : Fragment(), IBaseView {
    private val tag = BaseFragment::class.java.simpleName
    protected lateinit var mViewBinding: V
    protected lateinit var mViewModel: VM
    protected var mStackManager: StackManager = StackManager.getInstance()
    protected lateinit var mActivity: BaseActivity<*, *>

    init {
        initParam()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.i(tag, this::class.java.simpleName + " -> onAttach")
        mActivity = context as BaseActivity<*, *>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(tag, this::class.java.simpleName + " -> onCreate")
        createViewModel()
        Log.i(tag, this::class.java.simpleName + " -> onCreate 执行完毕")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Log.i(tag, this::class.java.simpleName + " -> onCreateView")
        mViewBinding = DataBindingUtil.inflate(inflater, initLayoutId(), container, false)
        Log.i(tag, this::class.java.simpleName + " -> onCreateView 执行完毕")
        return mViewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(tag, this::class.java.simpleName + " -> onViewCreated")
        initView()
        initObserver()
        initData()
        Log.i(tag, this::class.java.simpleName + " -> onViewCreated 执行完毕")
        viewBindingModel()
    }

    override fun onStart() {
        super.onStart()
        Log.i(tag, this::class.java.simpleName + " -> onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i(tag, this::class.java.simpleName + " -> onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(tag, this::class.java.simpleName + " -> onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(tag, this::class.java.simpleName + " -> onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i(tag, this::class.java.simpleName + " -> onDestroyView")
        if (Objects.nonNull(mViewBinding)) mViewBinding.unbind()
        lifecycle.removeObserver(mViewModel)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, this::class.java.simpleName + " -> onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.i(tag, this::class.java.simpleName + " -> onDetach")
    }

    /**
     * 创建ViewModel.
     */
    private fun createViewModel() {
        val modelClass: Class<BaseViewModel<*, *>>
        val type = javaClass.genericSuperclass
        modelClass = if (type is ParameterizedType) {
            type.actualTypeArguments[1] as Class<BaseViewModel<*, *>>
        } else {
            BaseViewModel::class.java
        }
        mViewModel = createViewModel(modelClass as Class<VM>)
    }

    /**
     * 创建ViewModel实例.
     */
    private fun <T : ViewModel> createViewModel(cls: Class<T>?): T {
        return ViewModelProvider(this)[cls!!]
    }

    /**
     * 绑定ViewModel.
     */
    private fun viewBindingModel() {
        mViewBinding.setVariable(initVariableId(), mViewModel)
        mViewBinding.lifecycleOwner = this
        lifecycle.addObserver(mViewModel)
        mViewModel.attachView(this)
    }

    /**
     * 展示加载框.
     */
    fun showLoading() {
        mActivity.showLoading()
    }

    /**
     * 关闭加载框.
     */
    fun closeLoading() {
        mActivity.closeLoading()
    }

    /**
     * 隐藏键盘.
     */
    fun hideInput() {
        mActivity.hideInput()
    }

    /**
     * 添加Fragment.
     * @param fragment 需要添加的fragment
     */
    protected fun addFragment(fragment: BaseFragment<*, *>) {
        mActivity.addFragment(fragment)
    }

    /**
     * 关闭当前Fragment.
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    protected fun closeFragment(next: Boolean) {
        mActivity.closeFragment(next)
    }

    /**
     * 关闭指定的Fragment.
     * @param fragment 需要关闭的Fragment
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    fun closeFragment(
        fragment: BaseFragment<*, *>?,
        next: Boolean
    ) {
        mActivity.closeFragment(fragment, next)
    }

    /**
     * 关闭指定的Fragment.
     * @param index 需要关闭的Fragment的下标
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    fun closeFragment(
        index: Int,
        next: Boolean
    ) {
        mActivity.closeFragment(index, next)
    }

    protected fun closeAllFragment() {
        mActivity.closeAllFragment()
    }
}