package com.android.base

import android.util.Log

open abstract class BaseModel<VM : IBaseViewModel> : IBaseModel {
    private val tag: String = BaseModel::class.java.simpleName
    protected lateinit var mViewModel: VM

    init {
        initParam()
    }

    override fun onAttachViewModel(baseViewModel: IBaseViewModel) {
        Log.i(tag, this::class.java.simpleName + " -> 绑定Model")
        mViewModel = baseViewModel as VM
    }

    override fun onCreate() {
        Log.i(tag, this::class.java.simpleName + " -> 执行 onCreate")
    }

    override fun onStart() {
        Log.i(tag, this::class.java.simpleName + " -> 执行 onStart")
    }

    override fun onDestroy() {
        Log.i(tag, this::class.java.simpleName + " -> 执行 onDestroy")
    }
}