package com.android.base

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import java.lang.IndexOutOfBoundsException
import java.util.Objects
import java.util.Stack

/**
 * Activity、Fragment的存储栈.
 */
class StackManager {
    private val activityStack = Stack<BaseActivity<*, *>>()
    private val fragmentStack = Stack<BaseFragment<*, *>>()

    /**
     * View压栈处理.
     */
    fun push(baseView: IBaseView) {
        Objects.requireNonNull(baseView)
        if (baseView is BaseActivity<*, *>) activityStack.push(baseView)
        if (baseView is BaseFragment<*, *>) fragmentStack.push(baseView)
    }

    /**
     * Activity出栈处理.
     */
    fun popActivity() {
        if (!activityStack.isEmpty()) activityStack.pop().finish()
    }

    /**
     * Fragment出栈处理.
     */
    fun popFragment(): BaseFragment<*, *>? {
        if (fragmentStack.isEmpty()) return null
        return fragmentStack.pop()
    }

    /**
     * 获取当前activity.
     */
    fun currentActivity(): BaseActivity<*, *>? {
        if (activityStack.isEmpty()) return null
        return activityStack.peek()
    }

    /**
     * 获取当前fragment.
     */
    fun currentFragment(): BaseFragment<*, *>? {
        if (fragmentStack.isEmpty()) return null
        return fragmentStack.peek()
    }

    /**
     * 移除指定activity.
     */
    fun removeActivity(activity: BaseActivity<*, *>) {
        if (activityStack.isEmpty() || Objects.isNull(activity)) return
        activityStack.remove(activity)
    }

    /**
     * 移除指定位置的activity.
     */
    fun removeActivity(index: Int) {
        if (activityStack.isEmpty() || 0 > index || index > activityStack.size) return
        activityStack[index].finish()
    }

    /**
     * 移除指定的fragment.
     */
    fun removeFragment(fragment: BaseFragment<*, *>) {
        if (!fragmentStack.isEmpty()) fragmentStack.remove(fragment)
    }

    /**
     * 移除指定位置的fragment.
     */
    fun removeFragment(index: Int) {
        if (!fragmentStack.isEmpty() && 0 < index && index < fragmentStack.size) fragmentStack.removeAt(
            index
        )
    }

    /**
     * 移除指定下标之后的所有activity.
     */
    fun removeAsIndexAfter(index: Int) {
        if (activityStack.isEmpty() || 0 > index || index > activityStack.size) return
        var activitys: MutableList<BaseActivity<*, *>> =
            activityStack.subList(index, activityStack.size)
        for (activity in activitys) {
            activity.finish()
        }
        activitys.clear()
    }

    /**
     * 移除指定下标之前的所有activity.
     */
    fun removeAsIndexBefore(index: Int) {
        if (activityStack.isEmpty() || 0 > index || index > activityStack.size) return
        var activitys: MutableList<BaseActivity<*, *>> = activityStack.subList(0, index)
        for (activity in activitys) {
            activity.finish()
        }
        activitys.clear()
    }


    /**
     * 移除指定下标之后的所有Fragment.
     */
    fun removeFsIndexAfter(index: Int) {
        if (fragmentStack.isEmpty() || 0 > index || index > fragmentStack.size) return
        fragmentStack.subList(index, fragmentStack.size).clear()
    }

    /**
     * 移除指定下标之前的所有Fragment.
     */
    fun removeFsIndexBefore(index: Int) {
        if (fragmentStack.isEmpty() || 0 > index || index > fragmentStack.size) return
        fragmentStack.subList(0, index).clear()
    }

    /**
     * 清除栈中所有的activity.
     */
    fun removeAllActivity() {
        if (activityStack.isEmpty()) return
        var baseActivityList: MutableList<BaseActivity<*, *>> = ArrayList()
        baseActivityList.addAll(activityStack)
        for (activity in baseActivityList) {
            activity.finish()
        }
        baseActivityList.clear()
        activityStack.removeAllElements()
    }

    /**
     * 清除栈中所有的fragment.
     */
    fun removeAllFragment() {
        fragmentStack.removeAllElements()
    }

    fun getActivityStackSize(): Int {
        return activityStack.size
    }

    fun getFragmentStackSize(): Int {
        if (fragmentStack.isEmpty()) return 0
        return fragmentStack.size
    }

    fun getActivityStack(): Stack<BaseActivity<*, *>> {
        return activityStack
    }

    fun getFragmentStack(): Stack<BaseFragment<*, *>> {
        return fragmentStack
    }

    /**
     * 是否包含某个组件.
     */
    fun isContain(baseView: IBaseView): Boolean {
        if (baseView is BaseActivity<*, *>) return activityStack.contains(baseView)
        if (baseView is BaseFragment<*, *>) return fragmentStack.contains(baseView)
        return false
    }

    /**
     * 是否包含某个组件且返回存在的index.它是以0为基数.
     */
    fun indexOf(baseView: IBaseView): Int {
        if (baseView is BaseActivity<*, *> && activityStack.contains(baseView)) return activityStack.indexOf(
            baseView
        )
        if (baseView is BaseFragment<*, *> && fragmentStack.contains(baseView)) return fragmentStack.indexOf(
            baseView
        )
        return -1
    }

    /**
     * 返回对象最后一次出现的位置，它是以0为基数.
     */
    fun lastIndexOf(baseView: IBaseView): Int {
        if (!Objects.isNull(baseView)) {
            if (baseView is BaseActivity<*, *> && activityStack.contains(baseView)) return activityStack.lastIndexOf(
                baseView
            )
            if (baseView is BaseFragment<*, *> && fragmentStack.contains(baseView)) return fragmentStack.lastIndexOf(
                baseView
            )
        }
        return -1
    }

    /**
     * 返回对象在堆栈中的位置距离堆栈顶部的距离,倒叙,它是以1为基数.
     * 一般情况下不适用此方法
     */
    fun search(baseView: IBaseView): Int {
        if (!Objects.isNull(baseView)) {
            if (baseView is BaseActivity<*, *>) {
                return activityStack.search(baseView)
            }
            if (baseView is BaseFragment<*, *>) {
                return fragmentStack.search(baseView)
            }
        }
        return -1
    }

    fun searchActivity(index: Int): BaseActivity<*, *> {
        if (index >= getActivityStackSize()) return throw IndexOutOfBoundsException("Index exceeds the maximum length of the set")
        return activityStack[index]
    }

    fun searchFragment(index: Int): BaseFragment<*, *> {
        if (index >= getFragmentStackSize()) throw IndexOutOfBoundsException("Index exceeds the maximum length of the set")
        return fragmentStack[index]
    }


    /**
     * 添加Fragment,如果不包含,压栈处理且显示,如果栈里包含该Fragment则直接展示不在进行压栈,且清除该Fragment之后的所有Fragment
     * @param fragment show fragment
     * @param fragmentId view id
     */
    fun addFragment(
        fragmentManager: FragmentManager,
        fragmentId: Int,
        fragment: BaseFragment<*, *>
    ) {
        if (0 == fragmentId) return
        var transaction: FragmentTransaction = fragmentManager.beginTransaction()
        for (fra in fragmentStack) transaction.hide(fra)
        if (!isContain(fragment)) {
            push(fragment)
            transaction.add(fragmentId, fragment).addToBackStack(null)
            transaction.show(fragment)
        } else {
            var searchPosition = lastIndexOf(fragment)
            removeFsIndexAfter(searchPosition)
            transaction.show(fragment)
        }
        transaction.commitAllowingStateLoss()
    }

    /**
     * 关闭Fragment,默认关闭栈顶的Fragment.
     */
    fun closeFragment(fragmentManager: FragmentManager, next: Boolean) {
        var fragment: BaseFragment<*, *>? = popFragment()
        if (Objects.isNull(fragment)) return
        var transaction: FragmentTransaction = fragmentManager.beginTransaction()
        transaction.hide(fragment!!)
        if (next) {
            var fragment: BaseFragment<*, *>? = currentFragment()
            if (Objects.nonNull(fragment)) fragment?.let { transaction.show(it) }
        }
        transaction.commitAllowingStateLoss()
    }

    /**
     * 关闭指定的Fragment.
     * @param fragment hide fragment
     * @param next 是否需要前一个Fragment
     */
    fun closeFragment(
        fragment: BaseFragment<*, *>?,
        fragmentManager: FragmentManager,
        next: Boolean
    ) {
        if (Objects.isNull(fragment)) return
        var transaction: FragmentTransaction = fragmentManager.beginTransaction()
        transaction.hide(fragment!!)
        if (next) {
            var fragment: BaseFragment<*, *>? = currentFragment()
            if (Objects.nonNull(fragment)) fragment?.let { transaction.show(it) }
        }
        transaction.commitAllowingStateLoss()
        removeFragment(fragment)
    }

    /**
     * 关闭指定的Fragment.
     * @param fragment hide fragment
     * @param next 是否需要前一个Fragment
     */
    fun closeFragment(
        index: Int,
        fragmentManager: FragmentManager,
        next: Boolean
    ) {
        var fragment: BaseFragment<*, *> = searchFragment(index)
        var transaction: FragmentTransaction = fragmentManager.beginTransaction()
        transaction.hide(fragment!!)
        if (next) {
            var fragment: BaseFragment<*, *>? = currentFragment()
            if (Objects.nonNull(fragment)) fragment?.let { transaction.show(it) }
        }
        transaction.commitAllowingStateLoss()
        removeFragment(index)
    }

    fun closeAllFragment(fragmentManager: FragmentManager) {
        var transaction: FragmentTransaction = fragmentManager.beginTransaction()
        for (fra in fragmentStack) transaction.hide(fra)
        transaction.commitAllowingStateLoss()
        removeAllFragment()
    }

    /**
     * 释放资源,销毁管理栈.
     * 应用程序退出时、崩溃时
     */
    fun destroy() {
        removeAllFragment()
        removeAllActivity()
    }

    companion object {
        fun getInstance() = Helper.instance
    }

    object Helper {
        val instance = StackManager()
    }
}