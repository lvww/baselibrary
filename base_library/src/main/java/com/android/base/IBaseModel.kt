package com.android.base

interface IBaseModel {
    fun initParam(){}
    fun onAttachViewModel(baseViewModel: IBaseViewModel)

    fun onCreate()

    fun onStart()

    fun onDestroy()
}