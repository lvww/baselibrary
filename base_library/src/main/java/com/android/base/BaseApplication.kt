package com.android.base

import android.app.Application
import android.content.res.Configuration
import android.util.Log

open class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Log.d("BaseApplication", this.javaClass.simpleName + " application start")
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Log.d("BaseApplication", this.javaClass.simpleName + " Current memory is low")
    }

    //程序在内存清理的时候被执行
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d("BaseApplication", this.javaClass.simpleName + " The system is clearing cache")
    }

    //系统设置被修改时调用
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.d("BaseApplication", this.javaClass.simpleName + " The system settings have been modified")
    }


    //程序终止的时候被调用，该函数不一定会执行
    override fun onTerminate() {
        super.onTerminate()
        Log.d("BaseApplication", this.javaClass.simpleName + " application termination")
        StackManager.getInstance().destroy()
    }
}