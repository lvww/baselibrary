package com.android.base.action

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import java.util.Objects
import java.util.concurrent.TimeUnit

/**
 * 点击事件
 */
@SuppressLint("CheckResult")
@BindingAdapter("clickCommand")
fun clickCommand(view: View, action: Action) {
    RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe { action.call() }
}

/*    @BindingAdapter(value = ["onClickCommand"], requireAll = false)
    fun onClickCommand(view: View, action: BindingAction<*, *>) {
        RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe { action.call() }
    }*/

/**
 * 点击事件,可返回Tag值.
 */
@SuppressLint("CheckResult")
@BindingAdapter("clickCommandTag")
fun <T> clickCommand(view: View, action: Action1<T>) {
    RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe {
        val obj = view.tag
        if (Objects.nonNull(obj)) action.call(obj as T)
    }
}

/*    @BindingAdapter(value = ["onClickCommand"], requireAll = false)
    fun <T> onClickCommand(view: View, action: BindingAction<T, *>) {
        RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).map {
            val obj = view.tag
            if (ConvertUtils.isEmpty(obj)) return@map Observable.just<String>("") as T
            obj as T
        }
            .subscribe { o: T -> action.call(o) }
    }*/

/**
 * 点击事件,可返回自定义属性param的值.
 */
@SuppressLint("CheckResult")
@BindingAdapter("clickCommandParam", "param")
fun <P> clickCommand(view: View, action: Action2<P>, param: Any) {
    RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).map { param as P }
        .subscribe { p: P -> action.call(p) }
}

/*    @BindingAdapter(value = ["onClickCommand", "param"], requireAll = false)
    fun <P> onClickCommand(view: View, action: BindingAction<*, P>, param: Any) {
        RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).map {
            if (ConvertUtils.isEmpty(param)) return@map Observable.just<String>("") as P
            param as P
        }.subscribe { p: P -> action.call(p) }
    }*/

/**
 * 点击事件,可返回Tag和自定义属性Param的值.
 */
@SuppressLint("CheckResult")
@BindingAdapter("clickCommandTagParam", "param")
fun <T, P> clickCommand(view: View, action: Action3<T, P>, param: Any) {
    RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe {
        val obj = view.tag
        if (Objects.nonNull(obj) && Objects.nonNull(param)) action.call(
            obj as T,
            param as P
        )
    }
}

/*    @BindingAdapter(value = ["onClickCommand", "param"], requireAll = false)
    fun <T, P> onClickCommandTagParam(view: View, action: BindingAction<T, P>, param: Any) {
        RxView.clicks(view).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe {
            val obj = view.tag
            if (!ConvertUtils.isEmpty(obj) && !ConvertUtils.isEmpty(param)) action.call(
                obj as T,
                param as P
            )
        }
    }*/

/**
 * 长按点击事件.
 */
@SuppressLint("CheckResult")
@BindingAdapter("onLongClickCommand")
fun onLongClickCommand(view: View, action: Action) {
    RxView.longClicks(view).throttleFirst(500, TimeUnit.MILLISECONDS)
        .subscribe { action.call() }
}

/*    @BindingAdapter(value = ["onLongClickCommand", "param"], requireAll = false)
    fun onLongClickCommand(view: View, action: BindingAction<*, *>) {
        RxView.longClicks(view).throttleFirst(500, TimeUnit.MILLISECONDS)
            .subscribe { action.call() }
    }*/

@SuppressLint("CheckResult")
@BindingAdapter("textWatcher")
fun textChange(editText: EditText, action: Action1<String>) {
    RxTextView.textChanges(editText).subscribe { t: CharSequence? -> action.call(t.toString()) }
}

@BindingAdapter("imageRes")
fun loadImage(imageView: ImageView, resId: Int) {
    imageView.setImageResource(resId)
}

@BindingAdapter("imageRes")
fun loadImage(imageView: ImageView, bitmap: Bitmap?) {
    if (null != bitmap) imageView.setImageBitmap(bitmap)
}

@BindingAdapter("imageRes")
fun loadImage(imageView: ImageView, drawable: Drawable?) {
    if (null != drawable) imageView.setImageDrawable(drawable)
}

@BindingAdapter(value = ["img_url", "placeHolder", "error"], requireAll = false)
fun loadImageUrl(imgView: ImageView, imgUrl: String, placeHolder: Int, error: Int) {
    Glide.with(imgView.context).load(imgUrl).placeholder(placeHolder).error(error).into(imgView)
}
