package com.android.base.action

/**
 * 注册点击事件,携带返回值{View.getTag}
 */
interface Action1<T> {
    fun call(t: T)
}