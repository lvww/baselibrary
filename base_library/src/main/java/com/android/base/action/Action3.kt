package com.android.base.action

/**
 * 注册点击事件,携带返回值,自定义属性param{T:View.getTag, P:View.getParam}
 */
interface Action3<T, P> {
    fun call(t: T, p: P)
}
