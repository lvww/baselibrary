package com.android.base.action

/**
 * 注册点击事件
 */
interface Action {
    fun call()
}