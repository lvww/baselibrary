package com.android.base.action

/**
 * 注册点击事件,携带返回值,自定义属性param{View.getParam}
 */
interface Action2<P> {
    fun call(t: P)
}