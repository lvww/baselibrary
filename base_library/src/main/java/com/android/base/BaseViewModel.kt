package com.android.base

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleOwner

open abstract class BaseViewModel<V : IBaseView, M : IBaseModel>(application: Application) :
    AndroidViewModel(application), IBaseViewModel {
    private val tag: String = BaseViewModel::class.java.simpleName
    protected lateinit var mView: V
    protected lateinit var mModel: M
    protected val stackManager: StackManager = StackManager.getInstance()

    init {
        initParam()
    }

    override fun attachView(baseView: IBaseView) {
        mView = baseView as V
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.i(tag, this::class.java.simpleName + " -> onCreate")
        mModel = initModel() as M
        initData()
        mModel.onAttachViewModel(this)
        Log.i(tag, this::class.java.simpleName + " -> onCreate 执行完毕")
        mModel.onCreate()
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        Log.i(tag, this::class.java.simpleName + " -> 执行 onStart")
        mModel.onStart()
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.i(tag, this::class.java.simpleName + " -> 执行 onResume")
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)
        Log.i(tag, this::class.java.simpleName + " -> 执行 onPause")
    }

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        Log.i(tag, this::class.java.simpleName + " -> 执行 onStop")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        Log.i(tag, this::class.java.simpleName + " -> 执行 onDestroy")
        mModel.onDestroy()
    }
}