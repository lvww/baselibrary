package com.android.base

import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.reflect.ParameterizedType
import java.util.Objects

abstract class BaseActivity<V : ViewDataBinding, VM : BaseViewModel<*, *>> : AppCompatActivity(),
    IBaseView {
    private val tag = BaseActivity::class.java.simpleName
    private var fragmentManager: FragmentManager = supportFragmentManager
    protected val stackManager: StackManager = StackManager.getInstance()
    protected lateinit var mViewBinding: V
    protected lateinit var mViewModel: VM
    protected var fragmentId: Int = 0

    init {
        initParam()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(tag, this::class.java.simpleName + " -> onCreate")
        stackManager.push(this)
        mViewBinding = DataBindingUtil.setContentView(this, initLayoutId())
        fragmentId = initFragment()
        createViewModel()
        initView()
        initLoading()
        initObserver()
        initData()
        Log.i(tag, this::class.java.simpleName + " -> onCreate 执行后完毕")
        viewBindingModel()
    }

    override fun onStart() {
        super.onStart()
        Log.i(tag, this::class.java.simpleName + " -> onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i(tag, this::class.java.simpleName + " -> onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(tag, this::class.java.simpleName + " -> onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(tag, this::class.java.simpleName + " -> onStop")
    }

    override fun finish() {
        StackManager.getInstance().removeActivity(this)
        super.finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, this::class.java.simpleName + " -> onDestroy")
        if (Objects.nonNull(mViewBinding)) mViewBinding.unbind()
        lifecycle.removeObserver(mViewModel)
    }

    /**
     * 设置加载Fragment的容器Id.
     */
    open fun initFragment(): Int {
        return 0
    }

    /**
     * 创建ViewModel.
     */
    private fun createViewModel() {
        val modelClass: Class<BaseViewModel<*, *>>
        val type = javaClass.genericSuperclass
        modelClass = if (type is ParameterizedType) {
            type.actualTypeArguments[1] as Class<BaseViewModel<*, *>>
        } else {
            BaseViewModel::class.java
        }
        mViewModel = createViewModel(modelClass as Class<VM>)
    }

    /**
     * 创建ViewModel实例.
     */
    private fun <T : ViewModel> createViewModel(cls: Class<T>?): T {
        return ViewModelProvider(this)[cls!!]
    }

    /**
     * 绑定ViewModel.
     */
    private fun viewBindingModel() {
        mViewBinding.setVariable(initVariableId(), mViewModel)
        mViewBinding.lifecycleOwner = this
        lifecycle.addObserver(mViewModel)
        mViewModel.attachView(this)
    }

    /**
     * 初始化加载框.
     */
    fun initLoading() {

    }

    /**
     * 展示加载框.
     */
    fun showLoading() {

    }

    /**
     * 关闭加载框.
     */
    fun closeLoading() {

    }

    /**
     * 隐藏键盘.
     */
    fun hideInput() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
    }

    /**
     * 添加Fragment.
     * @param fragment 需要添加的fragment
     */
    fun addFragment(fragment: BaseFragment<*, *>) {
        if (0 == fragmentId) return
        stackManager.addFragment(fragmentManager, fragmentId, fragment)
    }

    /**
     * 关闭当前Fragment.
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    fun closeFragment(next: Boolean) {
        stackManager.closeFragment(fragmentManager, next)
    }

    /**
     * 关闭指定的Fragment.
     * @param fragment 需要关闭的Fragment
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    fun closeFragment(
        fragment: BaseFragment<*, *>?,
        next: Boolean
    ) {
        stackManager.closeFragment(fragment, fragmentManager, next)
    }

    /**
     * 关闭指定的Fragment.
     * @param index 需要关闭的Fragment的下标
     * @param next 关闭这个Fragment后是否显示下一个Fragment
     */
    fun closeFragment(
        index: Int,
        next: Boolean
    ) {
        stackManager.closeFragment(index, fragmentManager, next)
    }

    /**
     * 关闭所有Fragment.
     */
    fun closeAllFragment() {
        stackManager.closeAllFragment(fragmentManager)
    }
}