# BaseLibrary
为Android开发基类库,避免平时新建项目时每次都要重写大量代码，
项目中加入依赖后即可直接开发业务,无须关心项目架构,因为基类已经帮你实现

**implementation 'com.gitlab.lvww:baselibrary:1.0.0'**

## 仓库指令
```
cd existing_repo
git remote add origin https://gitlab.com/lvww/baselibrary.git
git branch -M main
git push -uf origin main
```

## 版本分支定义说明
版本号初始值为1.0.0

- [ ] 每新增一个大的功能版本号第一位加1 -> 2.0.0
- [ ] 每新增一个单功能版本号第二位加1 -> 1.1.0
- [ ] 修复bug或者小幅度修改版本号末尾加1 -> 1.0.1

## 1.0.0 类库简单介绍

```
* BaseActivity、BaseFragment、BaseViewModel、BaseModel
* ViewAdapter已实现View组件的点击监听绑定具备防抖功能 ImageView的资源加载 EditText的文案改变
* Action 点击事件响应接口
* Action1 点击事件响应接口且返回View的Tag值,也可用于EditTex的文案改变监听回调
* Action2 点击事件响应接口且返回自定义属性param的值(使用频率不高,针对特殊场景,例如SkinManager换肤,该API换肤利用View的Tag值进行切换,此时若想使用View的Tag显然是不满足的,就需要额外自定义属性)
* Action3 点击事件响应接口且发挥View的Tag值和自定义属性param的值(使用频率不高,针对特殊场景)
* StackManager为基类管理库,界面的显示与隐藏销毁依赖于StackManager
* BaseApplication 目前只支持内存检测及变化，后期会增加应用前后台监控,也会针对组件化进行定制
```

## BaseActivity、BaseFragment 介绍
```
override fun finish() 从定义界面销毁逻辑
fun initFragment(): Int 设置加载fragment容器
fun initLoading() 初始化loading框
fun showLoading() 展示loading框
fun closeLoading() 关闭loading框
fun hideInput() 隐藏键盘
fun addFragment(fragment: BaseFragment<*, *>) 添加一个Fragment
fun closeFragment(Boolean) 关闭一个Fragment,true:关闭当前Fragment后显示上一个Fragment,flase:关闭当前Fragment后不显示上一个Fragment
fun closeAllFragment() 关闭所有的Fragment
```

## IBaseView 介绍
```
fun initParam() {} 执行的第一个方法可再次初始化一些参数
fun initLayoutId(): Int
fun initVariableId(): Int
fun initView() 用于初始化一些View相关的信息
fun initObserver() {} 注册监听之类的.
fun initData() {} 初始化一些必要参数OnStart之前的最后一个方法
```

## StackManager 介绍
```
fun push(baseView: IBaseView) View压栈处理，会自动区分是Activity还是Fragment
fun popActivity() Activity出栈处理
fun popFragment(): BaseFragment<*, *>? Fragment出栈处理
fun currentActivity(): BaseActivity<*, *>? 获取当前activity
fun currentFragment(): BaseFragment<*, *>? 获取当前fragment
fun removeActivity(activity: BaseActivity<*, *>) 移除指定activity
fun removeActivity(index: Int) 移除指定位置的activity
fun removeFragment(fragment: BaseFragment<*, *>) 移除指定的fragment
fun removeFragment(index: Int) 移除指定位置的fragment
fun isContain(baseView: IBaseView): Boolean 是否包含指定的View，会自动区分是Activity还是Fragment
fun addFragment(fragmentManager: FragmentManager, fragmentId: Int, fragment: BaseFragment<*, *>) 添加Fragment,如果不包含,压栈处理且显示,如果栈里包含该Fragment则直接展示不在进行压栈,且清除该Fragment之后的所有Fragment
```