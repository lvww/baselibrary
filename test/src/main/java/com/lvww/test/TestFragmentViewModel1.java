package com.lvww.test;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;

import com.android.base.BaseViewModel;
import com.android.base.IBaseModel;
import com.android.utils.log.Logger;

public class TestFragmentViewModel1 extends BaseViewModel<TestFragment1, TestFragmentModel1> {
    private static final String TAG = TestFragmentViewModel1.class.getSimpleName();

    public ObservableField<String> observableField;
    public TestFragmentViewModel1(@NonNull Application application) {
        super(application);
        Logger.d(TAG, "ViewModel的 构造器 执行");
    }

    @Override
    public void initParam() {
        Logger.d(TAG, "ViewModel的 initParam 执行");
        observableField = new ObservableField<>("这是第二个Fragment");
    }

    @Nullable
    @Override
    public IBaseModel initModel() {
        Logger.d(TAG, "ViewModel的 initModel 执行");
        return new TestFragmentModel();
    }

    @Override
    public void initData() {
        Logger.d(TAG, "ViewModel的 initData 执行");
    }
}
