package com.lvww.test;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.android.base.BaseFragment;
import com.android.base.BaseViewModel;
import com.android.base.IBaseModel;
import com.android.base.StackManager;
import com.android.base.action.Action;
import com.android.base.action.Action1;
import com.android.base.action.Action2;
import com.android.base.action.Action3;
import com.android.utils.ConvertUtils;
import com.android.utils.log.Logger;

import java.util.Stack;

public class TestBaseViewModel extends BaseViewModel<TestActivity, TestActivityModel> {
    private static final String TAG = TestBaseViewModel.class.getSimpleName();
    public ObservableInt param = new ObservableInt(0);

    public ObservableInt imgRes = new ObservableInt(R.mipmap.ic_launcher);

    public ObservableField<String> stringObservableField = new ObservableField<>("测试双向绑定");

    public ObservableField<String> url = new ObservableField<>("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F12f18496-3075-4367-b599-ae390f0e0b37%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&amp;refer=http%3A%2F%2Fsafe-img.xhscdn.com&amp;app=2002&amp;size=f9999,10000&amp;q=a80&amp;n=0&amp;g=0n&amp;fmt=auto?sec=1708675784&amp;t=578edcb2683514ef4ccf9425b677f5f8");

    public TestBaseViewModel(@NonNull Application application) {
        super(application);
        Logger.d(TAG, "ViewModel的 构造器 执行");
    }

    @Override
    public void initParam() {
        Logger.d(TAG, "ViewModel的 initParam 执行");
    }

    @Override
    public void initData() {
        Logger.d(TAG, "ViewModel的 initData 执行");
    }

    @Nullable
    @Override
    public IBaseModel initModel() {
        Logger.d(TAG, "ViewModel的 initModel 执行");
        return new TestActivityModel();
    }

    public Action testAction = () -> Logger.d(TAG, "点击事件");

    public Action1<String> testActionTag = s -> Logger.d(TAG, "点击事件获取Tag值 -> " + s);

    public Action2<Integer> testActionParam = t -> Logger.d(TAG, "点击事件获取Param值 -> " + t);

    public Action3<String, Integer> testActionTagParam = (s, s2) -> Logger.d(TAG, "点击事件", "获取Tag值 -> " + s, "获取Param值 -> " + s2);

    public Action1<String> editAction = o -> Logger.i("lv", "文字改变 " + o);

    public Action actionText = () -> {
        mView.updateText();
        Logger.i("lv", "测试双向绑定 -> " + stringObservableField.get());
        stringObservableField.set("测试数据绑定");
        Logger.i("lv", "测试双向绑定 -> " + stringObservableField.get());
    };

    public Action testAddFragment = () -> {
        Stack<BaseFragment<?, ?>> baseFragmentStack = getStackManager().getFragmentStack();
        mView.addFragment(TestFragment.newInstance());
        for (BaseFragment baseFragment : baseFragmentStack) {
            Logger.d(TAG, "testAddFragment Stack -> " + baseFragment);
        }

        mView.addFragment(TestFragment1.newInstance());
        for (BaseFragment baseFragment : baseFragmentStack) {
            Logger.d(TAG, "testAddFragment Stack1 -> " + baseFragment);
        }
    };

    public Action testCloseFragment = () -> {
        mView.closeFragment(false);
        Stack<BaseFragment<?, ?>> baseFragmentStack = getStackManager().getFragmentStack();
        for (BaseFragment baseFragment : baseFragmentStack) {
            Logger.d(TAG, "testCloseFragment Stack -> " + baseFragment);
        }
    };

    public Action testCloseIndexFragment = () -> {
        Stack<BaseFragment<?, ?>> baseFragmentStack = getStackManager().getFragmentStack();
        if(ConvertUtils.isEmpty(baseFragmentStack)) return;
        mView.closeFragment(0, false);
        Logger.d(TAG, "baseFragmentStack size " + StackManager.Companion.getInstance().getFragmentStackSize());
    };

    public Action testCloseAllFragment = () -> {
        mView.closeAllFragment();
        //mView.finish();
        Logger.d(TAG, "baseFragmentStack size " + StackManager.Companion.getInstance().getFragmentStackSize());
    };
}
