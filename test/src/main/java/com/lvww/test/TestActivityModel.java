package com.lvww.test;

import com.android.base.BaseModel;
import com.android.base.BaseViewModel;
import com.android.utils.log.Logger;

public class TestActivityModel extends BaseModel<BaseViewModel> {
    private static final String TAG = TestActivityModel.class.getSimpleName();

    public TestActivityModel() {
        Logger.d(TAG, "Model的 构造器 执行");
    }

    @Override
    public void initParam() {
        Logger.d(TAG, "Model的 initParam 执行");
    }

    @Override
    public void onCreate() {
        Logger.d(TAG, "Model的 onCreate 执行");
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onDestroy() {
        Logger.d(TAG, "Model的 onDestroy 执行");
    }
}
