package com.lvww.test;

import com.android.base.BaseModel;
import com.android.utils.log.Logger;

public class TestFragmentModel1 extends BaseModel<TestFragmentViewModel1> {
    private static final String TAG = TestFragmentModel1.class.getSimpleName();

    public TestFragmentModel1() {
        Logger.d(TAG, "Model的 构造器 执行");
    }

    @Override
    public void initParam() {
        super.initParam();
        Logger.d(TAG, "Model的 initParam 执行");
    }

    @Override
    public void onCreate() {
        Logger.d(TAG, "Model的 onCreate 执行");
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onDestroy() {
        Logger.d(TAG, "Model的 onDestroy 执行");
    }
}
