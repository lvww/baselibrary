package com.lvww.test;

import com.android.base.BaseFragment;
import com.lvww.test.databinding.FragmentTestBaseBinding;

public class TestFragment extends BaseFragment<FragmentTestBaseBinding, TestFragmentViewModel> {
    public static TestFragment newInstance() {
        return new TestFragment();
    }

    @Override
    public int initLayoutId() {
        return R.layout.fragment_test_base;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public void initView() {

    }
}
