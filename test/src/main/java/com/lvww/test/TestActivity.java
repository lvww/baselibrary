package com.lvww.test;

import com.android.base.BaseActivity;
import com.android.utils.log.Logger;
import com.lvww.test.databinding.ActivityTestBaseBinding;

public class TestActivity extends BaseActivity<ActivityTestBaseBinding, TestBaseViewModel> {
    private static final String TAG = TestActivity.class.getSimpleName();
    @Override
    public void initParam() {
        super.initParam();
        Logger.d(TAG, "view的 initParam 执行");
    }

    @Override
    public int initLayoutId() {
        return R.layout.activity_test_base;
    }

    @Override
    public int initFragment() {
        return R.id.fragment_id;
    }

    @Override
    public int initVariableId() {
        return BR.ViewModel;
    }

    @Override
    public void initView() {
        Logger.d(TAG, "view的 initView 执行");
    }

    @Override
    public void initObserver() {
        super.initObserver();
        Logger.d(TAG, "view的 initObserver 执行");
    }

    @Override
    public void initData() {
        super.initData();
        Logger.d(TAG, "view的 initData 执行");
    }

    public void updateText() {
        mViewBinding.testBindingText.setText("改变数据");
    }
}