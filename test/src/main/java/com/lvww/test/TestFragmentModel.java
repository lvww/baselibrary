package com.lvww.test;

import com.android.base.BaseModel;
import com.android.utils.log.Logger;

public class TestFragmentModel extends BaseModel<TestFragmentViewModel> {
    private static final String TAG = TestFragmentModel.class.getSimpleName();

    public TestFragmentModel() {
        Logger.d(TAG, "Model的 构造器 执行");
    }

    @Override
    public void initParam() {
        super.initParam();
        Logger.d(TAG, "Model的 initParam 执行");
    }

    @Override
    public void onCreate() {
        Logger.d(TAG, "Model的 onCreate 执行");
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onDestroy() {
        Logger.d(TAG, "Model的 onDestroy 执行");
    }
}
