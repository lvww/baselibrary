package com.lvww.test;


import com.android.base.BaseFragment;
import com.lvww.test.databinding.FragmentTestBase1Binding;

public class TestFragment1 extends BaseFragment<FragmentTestBase1Binding, TestFragmentViewModel1> {
    public static TestFragment1 newInstance() {
        return new TestFragment1();
    }

    @Override
    public int initLayoutId() {
        return R.layout.fragment_test_base_1;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public void initView() {

    }
}
